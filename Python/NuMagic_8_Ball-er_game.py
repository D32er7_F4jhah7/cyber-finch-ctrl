#This is a program for the Magic 8 Ball(er) game or fortune teller.

import random
def getAnswer(answerNumber):
    if answerNumber == 1:
        return 'It is certain'
    elif answerNumber == 2:
        return 'It is decidedly so'
    elif answerNumber == 3:
        return 'Yes'
    elif answerNumber == 4:
        return 'Reply hazy, try again'
    elif answerNumber == 5:
        return 'Ask again later'
    elif answerNumber == 6:
        return 'Conentrate and ask again'
    elif answerNumber == 7:
        return 'My reply is NO'
    elif answerNumber == 8:
        return 'Outlook not so good'
    elif answerNumber == 9:
        return 'Hmm, Very Doubtful'

print(getAnswer(random.randint(1, 9)))

#r = random.randint(1,9)
#fortune = getAnswer(r)
#print(fortune)
