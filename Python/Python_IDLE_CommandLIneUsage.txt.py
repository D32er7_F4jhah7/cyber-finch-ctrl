Startup and code execution

Upon startup with the -s option, IDLE will execute the file referenced by the environment variables IDLESTARTUP or PYTHONSTARTUP. IDLE first checks for IDLESTARTUP; if IDLESTARTUP is present the file referenced is run.  If IDLESTARTUP is not present, IDLE checks for PYTHONSTARTUP.  Files referenced by these environment variables are convenient places to store functions that are used frequently from the IDLE shell, or for executing import statements to import common modules.

In addition, Tk also loads a startup file if it is present.  Note that the Tk file is loaded unconditionally.  This additional file is .Idle.py and is looked for in the user’s home directory.  Statements in this file will be executed in the Tk namespace, so this file is not useful for importing functions to be used from IDLE’s Python shell.

Command line usage

idle.py [-c command] [-d] [-e] [-h] [-i] [-r file] [-s] [-t title] [-] [arg] ...

-c command  run command in the shell window
-d          enable debugger and open shell window
-e          open editor window
-h          print help message with legal combinations and exit
-i          open shell window
-r file     run file in shell window
-s          run $IDLESTARTUP or $PYTHONSTARTUP first, in shell window
-t title    set title of shell window
-           run stdin in shell (- must be last option before args)

If there are arguments:

* If -, -c, or r is used, all arguments are placed in sys.argv[1:...] and sys.argv[0] is set to '', '-c', or '-r'.  No editor window is opened, even if that is the default set in the Options dialog.
* Otherwise, arguments are files opened for editing and sys.argv reflects the arguments passed to IDLE itself.

Startup failure

IDLE uses a socket to communicate between the IDLE GUI process and the user code execution process.  A connection must be established whenever the Shell starts or restarts.  (The latter is indicated by a divider line that says ‘RESTART’). If the user process fails to connect to the GUI process, it displays a Tk error box with a ‘cannot connect’ message that directs the user here.  It then exits.

A common cause of failure is a user-written file with the same name as a standard library module, such as random.py and tkinter.py. When such a file is located in the same directory as a file that is about to be run, IDLE cannot import the stdlib file.  The current fix is to rename the user file.

Though less common than in the past, an antivirus or firewall program may stop the connection.  If the program cannot be taught to allow the connection, then it must be turned off for IDLE to work.  It is safe to allow this internal connection because no data is visible on external ports.  A similar problem is a network mis-configuration that blocks connections.

Python installation issues occasionally stop IDLE: multiple versions can clash, or a single installation might need admin access.  If one undo the clash, or cannot or does not want to run as admin, it might be easiest to completely remove Python and start over.

A zombie pythonw.exe process could be a problem.  On Windows, use Task Manager to check for one and stop it if there is.  Sometimes a restart initiated by a program crash or Keyboard Interrupt (control-C) may fail to connect.  Dismissing the error box or using Restart Shell on the Shell menu may fix a temporary problem.

When IDLE first starts, it attempts to read user configuration files in ~/.idlerc/ (~ is one’s home directory).  If there is a problem, an error message should be displayed.  Leaving aside random disk glitches, this can be prevented by never editing the files by hand.  Instead, use the configuration dialog, under Options.  Once there is an error in a user configuration file, the best solution may be to delete it and start over with the settings dialog.

If IDLE quits with no message, and it was not started from a console, try starting it from a console or terminal (python -m idlelib) and see if this results in an error message.   
