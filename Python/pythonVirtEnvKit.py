#For this reason, developers often create a virtual environment #for a project. A virtual

# environment is a subfolder in a project that contains a copy of a specific interpreter.

# When you activate the virtual environment, any packages you install are installed only in thqt environment.

# subfolder. When you then run a Python program within that environment, you know that it's
# running against only those specific packages.

# Note: While it's possible to open a virtual environment folder as a workspace, doing so
# is not recommended and might cause issues with using the Python extension.

# To create a virtual environment, use the following command, where ".venv" is the name
# of the environment folder:

# macOS/Linux
# You may need to run sudo apt-get install python3-venv first
python3 -m venv .venv

# Windows
# You can also use py -3 -m venv .venv
python -m venv .dinkbin

# -------------------------------------------------------------
